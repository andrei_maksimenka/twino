export const state = () => ({
  groups: [],
  answers: [],
})

export const getters = {
  getGroups: (state) => {
    return state.groups
  },
}

export const mutations = {
  ADD_GROUPS(state, data) {
    state.groups = data
  },
  ADD_ANSWER(state, answer) {

    const currentGroup = state.groups.find(group => group.title === answer.title) 
    const currentQuestion = currentGroup?.questions?.find(question => answer.question === question.question)
    
    if(currentQuestion){
      currentQuestion.selectedAnswer = answer.selectedAnswer
    }

    // state.groups.forEach((e) => {
    //   if (e.title === answer.title) {
    //     e.questions.forEach((e) => {
    //       if (answer.question === e.question) {
    //         e.selectedAnswer = answer.selectedAnswer
    //       }
    //     })
      // }
    // })
  },
}

export const actions = {
  getData({ commit }) {
    this.$axios.get('http://localhost:3001/suitability').then((res) => {
      const result = res.data.groups.map((e) => {
        const element = {
          ...e,
          questions: e.questions.map((e) => {
            return {
              ...e,
              selectedAnswer: null,
            }
          }),
        }
        return element
      })
      commit('ADD_GROUPS', result)
    })
  },
}
